﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Azure.Management.Compute;
using Microsoft.Azure.Management.Compute.Models;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Authentication;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Microsoft.Rest.Azure;

namespace TestApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private const string BLOB_IDENTIFIER = "blob.core.windows.net/";

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            var viewModel = new MainWindowViewModel();

            var view = new MainWindow();
            view.DataContext = viewModel;

            var credentials = GetAuthToken();
            var client = new ComputeManagementClient(credentials) { SubscriptionId = Constants.SubscriptionId };

            try
            {
                var virtualMachines = client.VirtualMachines.List(Constants.ResourceGroup);
                var targetVm = virtualMachines.First(vm => vm.Name.Equals("blobvm"));

                // These are user input preferences for "export"
                var vhdPrefix = "work-plox-v2";
                var destinationContainer = "blob-container";

                Task.Run(async () =>
                {
                    var result = await BeginCaptureVM(
                        client,
                        vhdPrefix,
                        destinationContainer,
                        targetVm.Name,
                        true);

                    if (result == null)
                    {
                        throw new Exception("Unable to initiate VM capture.");
                    }

                    // Account key will need to be input from the user as well.
                    // Generate blob for each disk.
                    var blobUri = GenerateBlobURI(targetVm, vhdPrefix, destinationContainer);
                    var storageCredentials = new StorageCredentials(GetStorageAccountFromURI(blobUri), "1ft3DvCJXmix+YlzkTljxeWJ2rDH4mEy6rsfeAcJoxTD8uKriRn+HdYuA170DBZ4IArEOpsUdxKE1HPggXBUOw==");
                    var cloudPageBlob = new CloudPageBlob(blobUri, storageCredentials);

                    cloudPageBlob.DownloadToFileAsync("C:\\test.vhd", FileMode.Create).GetAwaiter().GetResult();

                    /*foreach (var disk in targetVm.StorageProfile.DataDisks)
                    {
                        var blobUri = GenerateBlobURI(targetVm, vhdPrefix, destinationContainer, DiskType.DataDisk);
                        var storageCredentials = new StorageCredentials(GetStorageAccountFromURI(blobUri), "1ft3DvCJXmix+YlzkTljxeWJ2rDH4mEy6rsfeAcJoxTD8uKriRn+HdYuA170DBZ4IArEOpsUdxKE1HPggXBUOw==");
                        var cloudPageBlob = new CloudPageBlob(blobUri, storageCredentials);

                        cloudPageBlob.DownloadToFileAsync("C:\\test.vhd", FileMode.Create).GetAwaiter().GetResult();
                    }*/

                    // Clean up protocol? (Modifications to environment and may have cost associated)
                    // -> Spin up new VM with imaged VHD
                    // -> Delete the imaged VHD post acquisition
                    // -> Delete the generalized VM
                });
            }
            catch (Exception exception)
            {
                var test = exception;
            }
        }

        private async Task<AzureOperationResponse<VirtualMachineCaptureResult>> BeginCaptureVM(
            ComputeManagementClient client,
            string vhdPrefix,
            string destinationContainer,
            string vmName,
            bool overwriteVhDs = false)
        {
            var captureParameters = new VirtualMachineCaptureParameters
            {
                VhdPrefix = vhdPrefix,
                DestinationContainerName = destinationContainer,
                OverwriteVhds = overwriteVhDs,
            };

            var generalizeResult = client.VirtualMachines.GeneralizeWithHttpMessagesAsync(Constants.ResourceGroup, vmName).GetAwaiter().GetResult();
            if (generalizeResult.Response.StatusCode == HttpStatusCode.OK)
            {
                return await client.VirtualMachines.BeginCaptureWithHttpMessagesAsync(
                    Constants.ResourceGroup,
                    vmName,
                    captureParameters);
            }

            return null;
        }

        private Uri GenerateBlobURI(VirtualMachine targetVM, string prefix, string destinationContainer)
        {
            // https://cloudazurevmacquisition.blob.core.windows.net/vhds/blobvm20191213113720.vhd
            var baseImagePath = targetVM.StorageProfile.OsDisk.Vhd.Uri;
            var rootPathIndex = baseImagePath.IndexOf(BLOB_IDENTIFIER, StringComparison.OrdinalIgnoreCase);

            var rootPath = baseImagePath.Substring(0, rootPathIndex + BLOB_IDENTIFIER.Length);

            return new Uri($"{rootPath}system/Microsoft.Compute/Images/{destinationContainer}/{prefix}-osDisk.{targetVM.VmId}.vhd");
        }

        private string GetStorageAccountFromURI(Uri uri)
        {
            var uriString = uri.ToString();
            var protocol = uriString.IndexOf("/", StringComparison.OrdinalIgnoreCase) + 2;

            return uriString.Substring(protocol, BLOB_IDENTIFIER.Length + 1);
        }

        private AzureCredentials GetAuthToken()
        {
            // Needs to be changed to handle user profile authentication not tenant authentication.
            var context = new AuthenticationContext("https://login.microsoftonline.com/" + Constants.TenantId, false);
            var clientCredentials = new ClientCredential(Constants.ClientId, Constants.ClientSecret);
            var result = context.AcquireToken("https://management.core.windows.net/", clientCredentials);

            var tokenCredentials = new TokenCredentials(result.AccessToken);
            return new AzureCredentials(
                tokenCredentials,
                tokenCredentials,
                Constants.TenantId,
                AzureEnvironment.AzureGlobalCloud);
        }
    }
}
