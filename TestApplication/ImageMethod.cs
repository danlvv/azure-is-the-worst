﻿using System.Collections.Generic;
using Microsoft.Azure.Management.Compute.Models;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;

namespace TestApplication
{
    public class ImageMethod
    {
        /*var imageParameters = GenerateImageParameters(targetVm);
                    var imageCreationResponse = client.Images
                        .CreateOrUpdateWithHttpMessagesAsync(Constants.ResourceGroup, "newImage.vhd", imageParameters)
                        .GetAwaiter().GetResult();
                    targetImage = client.Images.List().First();*/

        private Image GenerateImageParameters(VirtualMachine vm)
        {
            return new Image
            {
                Location = vm.Location,
                Tags = vm.Tags,
                StorageProfile = new ImageStorageProfile
                {
                    OsDisk = new ImageOSDisk
                    {
                        BlobUri = vm.StorageProfile.OsDisk.Vhd.Uri,
                        OsState = OperatingSystemStateTypes.Generalized,
                        OsType = OperatingSystemTypes.Linux
                    },
                    DataDisks = new List<ImageDataDisk>(),
                    ZoneResilient = true
                },
                HyperVGeneration = "V1"
            };
        }

        private CloudBlob CopyImageToBlob(Image image)
        {
            // One of the methods to get a reference to the blob storage item in order to download it.
            var storageCredentials = new StorageCredentials();
            var cloudStorageAccount = new CloudStorageAccount(storageCredentials, useHttps: true);
            var blobClient = cloudStorageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("blob-container");

            return container.GetBlobReference("i ama blob");
        }
    }
}