﻿using System.Collections.ObjectModel;

namespace TestApplication
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            Collection = new ObservableCollection<string>
            {
                "Test 1",
                "Test 2",
                "Test 3",
                "Test 4",
                "Test 5",
            };
        }

        public ObservableCollection<string> Collection { get; }

        public string SelectedItem { get; set; }
    }
}